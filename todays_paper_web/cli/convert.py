import os
import argparse
import locale

from PIL import Image

from todays_paper_web.einkimage import register

register()


def main():
    locale.setlocale(locale.LC_ALL, "")
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-o",
        "--output",
        dest="output",
        type=argparse.FileType("wb"),
        default="output.epdiy",
        help="The destination filename",
    )
    parser.add_argument(
        "-f",
        "--format",
        dest="format",
        choices=["png", "waveshare", "epdiy"],
        help="The output file format. If not set it will be detected from the filename.",
    )
    parser.add_argument(
        "-i",
        "--input",
        dest="input",
        type=argparse.FileType("rb"),
        help="The input filename",
        required=True,
    )
    args = parser.parse_args()

    format = args.format
    if not format:
        _, ext = os.path.splitext(args.output.name)
        format = ext[1:]

    img = Image.open(args.input)
    img.save(args.output, format)


if __name__ == "__main__":
    main()
