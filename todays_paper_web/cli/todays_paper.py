import os
import argparse
import locale

from todays_paper_web.generator import Generator
from todays_paper_web.storage.local import LocalStorage
from todays_paper_web.einkimage import register

register()


def main():
    locale.setlocale(locale.LC_ALL, "")
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "config",
        type=str,
        help="The configuration file to parse",
    )
    parser.add_argument(
        "-o",
        "--output",
        dest="output",
        type=argparse.FileType("wb"),
        default="output.png",
        help="The destination filename",
    )
    parser.add_argument(
        "-f",
        "--format",
        dest="format",
        choices=["png", "waveshare", "epdiy"],
        help="The file format. If not set it will be detected from the filename.",
    )
    parser.add_argument(
        "-s",
        "--screen",
        dest="screen",
        help="The name of the screen to render. If not set the scheduler section will be used to calculate the active screen",
    )
    parser.add_argument(
        "-d",
        "--debug",
        action="store_true",
        dest="debug",
        help="Enable debugging",
    )
    args = parser.parse_args()
    filename = os.path.basename(args.config)

    format = args.format
    if not format:
        _, ext = os.path.splitext(args.output.name)
        format = ext[1:]

    storage = LocalStorage()

    generator = Generator(filename, storage)
    generator.debugging = args.debug
    screen = args.screen
    if not screen:
        timestamp, screen, _ = generator.getCurrentScreen()
        generator.setTimestamp(timestamp)

    img = generator.renderScreen(screen)
    if img:
        img.save(args.output, format)


if __name__ == "__main__":
    main()
