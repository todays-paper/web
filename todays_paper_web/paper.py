import logging
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from typing import Union
from datetime import datetime
from starlette.responses import Response
from PIL import Image

from todays_paper_web.server import Server

app = FastAPI()
server = Server()


class Paper(BaseModel):
    format: Union[str, None] = "png"
    uuid: str
    battery: Union[float, None] = 0.0
    temperature: Union[float, None]
    version: Union[str, None] = ""


@app.post("/paper")
async def paper_post(paper: Paper):
    screen, img = server.getCurrentScreen(paper.uuid, paper.format)
    if not img:
        logging.error("Screen %s was not found", paper.uuid)
        raise HTTPException(404, detail="Screen was not found")

    nextTs = server.getNextScreenTime(paper.uuid)
    timeToNext = nextTs - datetime.now(nextTs.tzinfo)
    headers = {"Cache-Control": f"max-age={int(timeToNext.total_seconds())}"}

    # Log the request
    server.logRequest(
        paper.uuid, screen, paper.battery, paper.temperature, paper.version
    )

    mime_type = Image.MIME[paper.format.upper()]
    return Response(content=img, media_type=mime_type, headers=headers)


@app.get("/paper")
async def paper_get(uuid: str, format: str = "png"):
    return await paper_post(Paper(uuid=uuid, format=format))


@app.on_event("startup")
def startup():
    logging.basicConfig(format="%(asctime)s %(message)s", level=logging.INFO)
    server.startup()
