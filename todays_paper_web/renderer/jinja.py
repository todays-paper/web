import xml.etree.ElementTree as ET
from datetime import datetime, timedelta
import logging

from jinja2 import Environment, BaseLoader, select_autoescape, exceptions
from todays_paper_web.storage.storage import Storage

from todays_paper_web.widgets import DataSource

_LOGGER = logging.getLogger(__name__)


class Loader(BaseLoader):
    def __init__(
        self,
        name: str,
        storage: Storage,
    ) -> None:
        super().__init__()
        self._name = name
        self._storage = storage

    def get_source(self, environment, template):
        source = self._storage.getFileBytes(self._name, template)
        return str(source, "utf-8"), None, lambda: True


class JinjaRenderer:
    def __init__(
        self, config: dict, sources: dict[str, DataSource], storage: Storage, name: str
    ):
        self._config = config
        self._sources = sources
        self._storage = storage
        self._name = name
        self._debugging = False

    @property
    def debugging(self) -> bool:
        return self._debugging

    @debugging.setter
    def debugging(self, debugging) -> None:
        self._debugging = debugging

    def render(self, width: int, height: int, timestamp: datetime) -> ET.Element:
        env = Environment(
            loader=Loader(self._name, self._storage),
            autoescape=select_autoescape(),
            trim_blocks=True,
            lstrip_blocks=True,
            line_statement_prefix="#",
        )
        template = env.get_template(self._config.get("template", ""))
        tomorrow = timestamp + timedelta(days=1)
        try:
            xml = template.render(
                height=height,
                source=self.source,
                timestamp=timestamp,
                tomorrow=tomorrow,
                timezone=timestamp.tzinfo,
                width=width,
            )
            if self.debugging:
                _LOGGER.warn(xml)
            return ET.fromstring(xml)
        except exceptions.UndefinedError as error:
            _LOGGER.error("Could not render Jinja template: %s", error)
        except ET.ParseError as error:
            _LOGGER.error("Could not render Jinja template: %s", error)
            _LOGGER.error(xml)
        return None

    def source(self, name):
        source = self._sources.get(name)
        if source:
            return source.fetch()
        _LOGGER.error('Could not find source "%s"', name)
        return {}
