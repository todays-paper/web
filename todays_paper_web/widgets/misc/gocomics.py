from gocomics import Comic
from PIL import Image
import requests
from io import BytesIO


from todays_paper_web.widgets import registerView
from todays_paper_web.widgets.core.image import ImageView


@registerView("gocomics")
class GoComics(ImageView):
    def fetch_img(self):
        comic = Comic(self.identifier)
        response = requests.get(comic.image)
        return Image.open(BytesIO(response.content))
