from typing import NewType
from PIL import Image

ImageType = NewType("Image", Image.Image)


class WidgetCollection(dict):
    def __call__(self, name):
        def decorator(widget):
            self[name] = widget
            return widget

        return decorator


# pylint: disable=invalid-name
registerDataSource = WidgetCollection()
registerView = WidgetCollection()

# pylint: disable=wrong-import-position
from .datasource import DataSource
from .view import View

# pylint: disable=wrong-import-position
from .core import image, layout, request, screen, text
from .misc import gocomics
from .weather import core, yr
from .calendar import google, ical
