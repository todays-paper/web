from __future__ import print_function

from icalendar import Calendar
import requests
import logging

from todays_paper_web.widgets import DataSource, registerDataSource

_LOGGER = logging.getLogger(__name__)


@registerDataSource("calendar.ical")
class ICalendarDataSource(DataSource):
    def fetch(self):
        r = requests.get(self.url)
        cal = Calendar.from_ical(r.text)
        return cal
