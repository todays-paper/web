from todays_paper_web.widgets import registerDataSource
from datetime import datetime


class DataSource:
    def __init__(self, params):
        self._params = params
        self._timestamp: datetime = datetime.today()

    def fetch(self):
        return {}

    @property
    def timestamp(self) -> datetime:
        return self._timestamp

    @timestamp.setter
    def timestamp(self, timestamp: datetime) -> None:
        self._timestamp = timestamp

    def __getattr__(self, name):
        return self._params.get(name)

    @staticmethod
    def get(name, params):
        # pylint: disable=invalid-name
        DataSourceCls = registerDataSource.get(name, DataSource)
        source = DataSourceCls(params)
        return source
