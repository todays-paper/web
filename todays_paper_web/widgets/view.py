from __future__ import annotations
import os
from pathlib import Path
from typing import BinaryIO, Iterator

from PIL import Image
import xml.etree.ElementTree as ET

import webcolors

from todays_paper_web.storage.storage import Storage
from todays_paper_web.widgets import registerView, ImageType
from .datasource import DataSource


class View:
    def __init__(self, params):
        self._element = None
        self._source = None
        self._params = params
        self._storage: Storage = None
        self._config: str = ""

    @property
    def children(self) -> Iterator[View]:
        for child in self._element:
            view = View.get(child.tag, None, child.attrib, self._storage, self._config)
            view.element = child
            yield view

    @property
    def element(self) -> ET.Element:
        return self._element

    @element.setter
    def element(self, element: ET.Element):
        self._element = element

    @property
    def innerText(self):
        return "".join(self._element.itertext())

    def getData(self):
        if not self.source:
            return {}
        return self.source.fetch()

    def openFile(self, filename) -> BinaryIO:
        return self._storage.getFileObject(self._config, filename)

    def render(self) -> ImageType:
        return Image.new("RGB", (0, 0), "white")

    @property
    def source(self):
        return self._source

    @source.setter
    def source(self, source):
        self._source = source

    def __getattr__(self, name):
        return self._params.get(name)

    @staticmethod
    def decodeColor(color: str, default: str) -> webcolors.IntegerRGB:
        try:
            return webcolors.name_to_rgb(color)
        except:
            pass
        try:
            return webcolors.hex_to_rgb(color)
        except:
            pass
        return webcolors.name_to_rgb(default)

    @staticmethod
    def get(name, source, params, storage, config) -> View:
        ViewCls = registerView.get(name, View)  # pylint: disable=invalid-name
        view = ViewCls(params)
        view.source = source
        view._storage = storage
        view._config = config
        return view

    @staticmethod
    def paste(target: ImageType, source: ImageType, position: tuple[int, int]):
        if source.mode == "RGBA":
            target.paste(source, position, source)
        else:
            target.paste(source, position)
