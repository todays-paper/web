from PIL import Image, ImageDraw, ImageFont

import textwrap

from todays_paper_web.fonts import Fonts
from todays_paper_web.widgets import registerView, View


@registerView("text")
class TextView(View):
    def render(self):
        # Format the text
        text = self.innerText

        # Get bounding box
        img = Image.new("1", (0, 0))
        draw = ImageDraw.Draw(img)
        font = Fonts.load("FiraSans", int(self.size or 18))
        if self.font:
            try:
                font = ImageFont.truetype(self.openFile(self.font), int(self.size))
            except:
                print("Could not load font", self.font)

        if self.width:
            # If width is specified, then word wrap the text within the bounds
            lines = TextView.wordwrap(text, int(self.width), draw, font)
            text = "\n".join(lines)

        _, _, width, height = draw.multiline_textbbox(
            (0, 0),
            text,
            font=font,
        )

        # Draw the text
        img = Image.new("RGBA", (width, height), (0, 0, 0, 0))
        draw = ImageDraw.Draw(img)
        color = TextView.decodeColor(self.color, "black")
        draw.multiline_text(
            (0, 0),
            text,
            font=font,
            fill=(color.red, color.green, color.blue, 255),
        )
        return img

    @staticmethod
    def wordwrap(
        text: str,
        width: int,
        draw: ImageDraw.ImageDraw,
        font: ImageFont.FreeTypeFont,
    ):
        wrapper = textwrap.TextWrapper()
        chunks = wrapper._split(text)

        words = {}
        lines = []
        line = []
        lineWidth = 0
        for word in chunks:
            # Cache the size if the word is used more than once
            if word not in words:
                _, _, wordWidth, _ = draw.textbbox((0, 0), word, font=font)
                words[word] = wordWidth

            if lineWidth + words[word] > width:
                lines.append(line)
                line = []
                lineWidth = 0
            line.append(word)
            lineWidth += words[word]
        lines.append(line)
        return ["".join(line).strip() for line in lines]
