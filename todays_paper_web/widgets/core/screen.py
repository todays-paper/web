from PIL import Image

from todays_paper_web.widgets import registerView, View
from .layout import BoxView


@registerView("screen")
class ScreenView(BoxView):
    def render(self):
        color = ScreenView.decodeColor(self.color, "white")
        img = Image.new("RGB", (self.width, self.height), color)
        box = super().render()
        ScreenView.paste(img, box, (0, 0))
        return img
