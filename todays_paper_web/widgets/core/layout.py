from re import I
from PIL import Image

from todays_paper_web.widgets import registerView, View


@registerView("box")
class BoxView(View):
    def render(self):
        width, height = 0, 0
        imgs = []
        # Loop all children to get total width and height of the box
        for child in self.children:
            childImg = child.render()
            if not childImg:
                continue
            x = int(child.x or 0)
            y = int(child.y or 0)
            align = child.align or "left"
            valign = child.valign or "top"
            if child.x:
                # Only align x here if we have x attribute
                if align == "right":
                    x -= childImg.width
                elif align == "center":
                    x -= int(childImg.width / 2.0)
            if child.y:
                # Only align y here if we have y attribute
                if valign == "bottom":
                    y -= childImg.height
                elif valign in ["middle", "center"]:
                    y -= int(childImg.height / 2.0)
            width = max(width, childImg.width + x)
            height = max(height, childImg.height + y)
            imgs.append((childImg, x, y, align, valign))
        if self.width:
            width = int(self.width)
        if self.height:
            height = int(self.height)
        img = Image.new("RGBA", (width, height), (0, 0, 0, 0))

        # Now we have the box width and height, loop all and render them
        for childImg, x, y, align, valign in imgs:
            if x == 0:
                # Do alignment here if no x attribute are set
                if align == "right":
                    x = width - childImg.width
                elif align == "center":
                    x = int((width - childImg.width) / 2.0)
            if y == 0:
                # Do alignment here if no y attribute are set
                if valign == "bottom":
                    y = height - childImg.height
                elif valign in ["middle", "center"]:
                    y = int((height - childImg.height) / 2.0)
            self.paste(img, childImg, (x, y))

        return img


@registerView("column")
class ColumnView(View):
    def render(self):
        width, height = 0, 0
        imgs = []
        for child in self.children:
            childImg = child.render()
            x = int(child.x or 0)
            width = max(width, childImg.width + x)
            height += childImg.height
            imgs.append((childImg, x))
        y = 0
        img = Image.new("RGBA", (width, height), (0, 0, 0, 0))
        for childImg, x in imgs:
            ColumnView.paste(img, childImg, (x, y))
            y += childImg.height
        return img


@registerView("row")
class RowView(View):
    def render(self):
        width, height = 0, 0
        imgs = []
        for child in self.children:
            childImg = child.render()
            y = int(child.y or 0)
            height = max(height, childImg.height + y)
            width += childImg.width
            imgs.append((childImg, child.y))
        x = 0
        img = Image.new("RGBA", (width, height), (0, 0, 0, 0))
        for childImg, y in imgs:
            if not y:
                if self.valign == "bottom":
                    y = height - childImg.height
                elif self.valign in ["middle", "center"]:
                    y = int((height - int(childImg.height)) / 2.0)

            self.paste(img, childImg, (x, y))
            x += childImg.width
        return img
