import requests
from todays_paper_web.widgets import registerDataSource, DataSource


@registerDataSource("request")
class RequestSource(DataSource):
    def fetch(self):
        auth = None
        if self.username:
            auth = requests.auth.HTTPBasicAuth(self.username, self.password)
        r = requests.get(self.url, auth=auth)
        contentType = r.headers["Content-Type"].split(";")
        if "application/json" in contentType:
            return r.json()
        return r.text
