import logging
from PIL import Image

from todays_paper_web.widgets import registerView, View

_LOGGER = logging.getLogger(__name__)


@registerView("image")
class ImageView(View):
    def render(self):
        img = self.fetch_img()
        if img.mode in ("LA"):
            img = img.convert("RGBA")

        new_width, new_height = img.size
        if self.width:
            new_width = int(self.width)
        if self.height:
            new_height = int(self.height)
        if new_width != img.size[0] or new_height != img.size[1]:
            img = self.resize(img, new_width, new_height)
        return img

    def fetch_img(self) -> Image:
        return Image.open(self.openFile(self.path))

    def resize(self, img: Image, box_width: int, box_height: int) -> Image:
        orig_width, orig_height = img.size
        aspect = orig_width / orig_height
        fillmode = str(self.fillmode or "preserveaspectfit").lower()
        new_width, new_height = img.size
        if fillmode in ("preserveaspectfit", "preserve_aspect_fit"):
            if orig_width > box_width:
                new_width = box_width
                new_height = round(box_width / orig_width * orig_height)
            if orig_height > box_height:
                new_width = round(box_height / orig_height * orig_width)
                new_height = box_height
        elif fillmode in ("preserveaspectstretch", "preserve_aspect_stretch"):
            if box_height * aspect <= box_width:
                new_width = round(box_height * aspect)
                new_height = box_height
            else:
                new_width = box_width
                new_height = round(box_width / aspect)

        else:
            _LOGGER.warning("Unknown fillmode %s", fillmode)
            return img
        return img.resize((new_width, new_height))
