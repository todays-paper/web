import asyncio
import csv
import logging
import os
from datetime import datetime, timedelta
from pathlib import Path

from .einkimage import register
from .generator import Generator
from .storage import Storage

_LOGGER = logging.getLogger(__name__)
CACHE_DIR = Path(os.environ.get("CACHE_DIR", "cache")).resolve()


class Runner:
    def __init__(self, generator: Generator, name: str) -> None:
        self._generator = generator
        self._name = name
        self._nextTime: datetime = None
        self._currentScreen: str = ""
        self._nextScreen: str = ""
        self._skew = 10

    def getCurrentScreen(self, format: str) -> tuple[str, bytes]:
        path = self.pathForFormat(format)
        with open(path, "rb") as file:
            return self._currentScreen, file.read()

    def getNextScreenTime(self) -> datetime:
        return self._nextTime

    def pathForFormat(self, format: str) -> str:
        return CACHE_DIR / f"{self._name}.{format}"

    async def render(self, screen: str, timestamp: datetime):
        _LOGGER.info(f"running {self._name}:{screen} for {timestamp}")
        self._generator.setTimestamp(timestamp)
        img = self._generator.renderScreen(screen)
        img.save(self.pathForFormat("png"), "png")
        img.save(self.pathForFormat("epdiy"), "epdiy")

    async def start(self):
        _LOGGER.info("Starting runner for %s", self._name)
        timestamp, self._currentScreen, _ = self._generator.getCurrentScreen()
        try:
            # Render screen on startup
            await self.render(self._currentScreen, timestamp)
        except Exception as e:
            _LOGGER.error(f"Could not render {self._name}: {e}")

        self._nextTime, self._nextScreen = self._generator.getNextScreen()
        _LOGGER.info(f"{self._nextTime}, {self._nextScreen}")

        while True:
            # Wait until it's time to pre-render next screen
            delay: timedelta = self._nextTime - datetime.now(self._nextTime.tzinfo)
            delay = delay - timedelta(minutes=self._skew)
            _LOGGER.info("Wait %s seconds", str(delay))
            await asyncio.sleep(delay.total_seconds())

            _LOGGER.info("Its time to pre-render next screen: %s", self._nextScreen)
            self._currentScreen = self._nextScreen
            await self.render(self._nextScreen, self._nextTime)
            self._nextTime, self._nextScreen = self._generator.getNextScreen(skip=1)
            _LOGGER.info(f"Will wait for {self._nextScreen} at {self._nextTime}")


class Server:
    def __init__(self):
        self._runners = {}
        self._storage: Storage = Storage.getInstance()

    def getCurrentScreen(self, uuid: str, format: str) -> tuple[str, bytes]:
        if uuid not in self._runners:
            return None, None

        return self._runners[uuid].getCurrentScreen(format)

    def getNextScreenTime(self, uuid: str) -> datetime:
        if uuid not in self._runners:
            return None
        return self._runners[uuid].getNextScreenTime()

    def loadGenerators(self):
        for name in self._storage.listConfigs():
            try:
                generator = Generator(name, self._storage)
                self._runners[name] = Runner(generator, name)
                asyncio.create_task(self._runners[name].start())
            except:
                logging.error(f"Could not load {name}")
                raise

    @staticmethod
    def logRequest(
        uuid: str, screen: str, battery: float, temperature: float, firmwareVersion: str
    ) -> None:
        path = CACHE_DIR / f"{uuid}.log"
        fieldnames = ["timestamp", "screen", "battery", "temperature", "firmware"]
        exists = path.exists()
        with open(path, "a", newline="") as csvfile:
            logwriter = csv.DictWriter(csvfile, fieldnames=fieldnames)
            if not exists:
                logwriter.writeheader()
            logwriter.writerow(
                {
                    "timestamp": datetime.now().isoformat(),
                    "screen": screen,
                    "battery": battery,
                    "temperature": temperature,
                    "firmware": firmwareVersion,
                }
            )

    def startup(self):
        if not CACHE_DIR.exists():
            CACHE_DIR.mkdir(parents=True)
        register()  # Register image types

        self.loadGenerators()
