from enum import Enum
from PIL import ImageFont
from pathlib import Path

BASE_PATH = (Path(__file__).parent / "otf").resolve()
FS_OTF = "FiraSans-Medium.otf"


class Fonts:
    @staticmethod
    def load(name: str, size=18):
        if name == "FiraSans":
            return ImageFont.truetype(f"{BASE_PATH / FS_OTF}", size)
        return ImageFont.truetype(f"{BASE_PATH / FS_OTF}", size)
