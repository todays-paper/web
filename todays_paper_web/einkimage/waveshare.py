import struct

MIME_TYPE = "image/eink"
EXTENSION = "eInk"

BLACK = 0
WHITE = 1
GRAY = 2
RED = 3


def getEInkPixelValue(rgb):
    if rgb[0] == 255 and rgb[1] <= 25 and rgb[2] <= 25:
        return RED
    if rgb[0] == 167 and rgb[1] == 167 and rgb[2] == 167:
        return GRAY
    if rgb[0] == 186 and rgb[1] == 186 and rgb[2] == 186:
        return GRAY
    if rgb[0] == 255 and rgb[1] == 255 and rgb[2] == 255:
        return WHITE
    if rgb[0] <= 25 and rgb[1] <= 25 and rgb[2] <= 25:
        return BLACK
    raise Exception("Unknown pixel value", rgb)


def save(img, fd, filename):
    width, height = img.size
    pixels = []
    if width % 4 != 0:
        raise Exception("Width must be in factor 4")
    for y in range(height):
        row = []
        for x in range(0, width, 4):
            einkPixel = 0
            for sub in range(4):
                einkPixel <<= 2
                pixel = img.getpixel((x + sub, y))
                einkPixel |= getEInkPixelValue(pixel)
            row.append(einkPixel)
        pixels.append(row)

    # Size, width, height, bpp, dataSize
    fd.write(struct.pack("IHHBI", 16, width, height, 2, int(width * height / 4)))
    for row in pixels:
        fd.write(bytes(row))
