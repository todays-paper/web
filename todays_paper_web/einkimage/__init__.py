from PIL import Image

from . import epdiy, waveshare


def register():
    for format in (epdiy, waveshare):
        Image.register_save(format.EXTENSION, format.save)
        Image.register_extension(format.EXTENSION, format.EXTENSION)
        Image.register_mime(format.EXTENSION, format.MIME_TYPE)
