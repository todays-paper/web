import struct

from PIL import Image

MIME_TYPE = "image/epdiy"
EXTENSION = "epdiy"


def save(img: Image, fd, filename):
    width, height = img.size
    img = img.convert("L", colors=16)
    pixels = []
    if width % 2 != 0:
        raise Exception("Width must be in factor 2")
    for y in range(height):
        for x in range(0, width, 2):
            pixel = (int)(img.getpixel((x + 1, y)) / 255.0 * 15) << 4
            pixel |= (int)(img.getpixel((x, y)) / 255.0 * 15)
            pixels.append((pixel))

    # Size, width, height, bpp, dataSize
    fd.write(struct.pack("IHHBI", 16, width, height, 4, int(width * height / 2)))
    fd.write(bytes(pixels))
