from typing import BinaryIO, Iterator
from io import BytesIO
import os

from .storage import Storage

import boto3


class S3Storage(Storage):
    BUCKET = os.environ.get("TP_S3_BUCKET", "todays-paper.ink")

    def __init__(self) -> None:
        self._client: boto3.s3 = boto3.client("s3")

    def listConfigs(self) -> Iterator[str]:
        response = self._client.list_objects(
            Bucket=S3Storage.BUCKET,
            Delimiter="/",
            Prefix="configs/",
        )
        for prefix in response.get("CommonPrefixes"):
            key = prefix.get("Prefix")
            yield key[8:-1]

    def getConfig(self, name: str) -> str:
        return str(self.getFileBytes(name, "config.yml"), "utf-8")

    def getFileBytes(self, config: str, name: str) -> bytes:
        file = self._client.get_object(
            Bucket=S3Storage.BUCKET, Key=f"configs/{config}/{name}"
        )
        body = file.get("Body")
        return body.read()

    def getFileObject(self, config: str, name: str) -> BinaryIO:
        return BytesIO(self.getFileBytes(config, name))
