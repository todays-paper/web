from __future__ import annotations
from abc import ABC, abstractmethod
from typing import BinaryIO, Iterator
import os


class Storage(ABC):
    instance = None

    @abstractmethod
    def listConfigs(self) -> Iterator[str]:
        pass

    @abstractmethod
    def getConfig(self, name: str) -> str:
        pass

    @abstractmethod
    def getFileBytes(self, config: str, name: str) -> bytes:
        pass

    @abstractmethod
    def getFileObject(self, config: str, name: str) -> BinaryIO:
        pass

    @staticmethod
    def getInstance() -> Storage:
        if Storage.instance is not None:
            return Storage.instance
        storageType = os.environ.get("TP_STORAGE", "local").lower()
        if storageType == "s3":
            Storage.instance = S3Storage()
        else:
            Storage.instance = LocalStorage()
        return Storage.instance


from .local import LocalStorage
from .s3 import S3Storage
