from pathlib import Path
import os
from typing import BinaryIO, Iterator

from .storage import Storage


class LocalStorage(Storage):
    dir = Path(os.environ.get("CONFIG_DIR", "configs"))

    def __init__(self) -> None:
        pass

    def listConfigs(self) -> Iterator[str]:
        for path in LocalStorage.dir.iterdir():
            if path.suffix != ".yml":
                continue
            yield path.stem

    def getConfig(self, name: str) -> str:
        filename = LocalStorage.dir / f"{name}.yml"
        with open(filename, "r") as f:
            return f.read()

    def getFileBytes(self, config: str, name: str) -> bytes:
        file = self.getFileObject(config, name)
        try:
            return file.read()
        finally:
            file.close()

    def getFileObject(self, config: str, name: str) -> BinaryIO:
        filename = LocalStorage.dir / name
        return open(filename, "rb")
