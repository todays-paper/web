"""Setup module for todays-paper-web"""

from setuptools import setup

setup(
    name="todays-paper-web",
    version="0.1",
    description="Todays paper web",
    author="Magnus Ahlberg",
    author_email="xxx@gmail.com",
    packages=["todays_paper_web"],
    entry_points={
        "console_scripts": [
            "todays_paper = todays_paper_web.cli.todays_paper:main",
            "tpconvert = todays_paper_web.cli.convert:main",
        ]
    },
)
