FROM python:3.9-alpine

ENV CONFIG_DIR=/configs
ENV CACHE_DIR=/cache
ENV PORT=8000
WORKDIR /app

VOLUME [ "/configs", "/cache" ]
EXPOSE $PORT
RUN adduser -D user
RUN mkdir -p /cache && chown user /cache && chmod 755 /cache

RUN apk add gcc make musl-dev zlib-dev jpeg-dev freetype-dev

COPY requirements.txt /app/
RUN pip install -r requirements.txt
ADD todays_paper_web todays_paper_web
ADD setup.py ./
RUN python setup.py install

USER user
CMD uvicorn todays_paper_web.paper:app --reload --host 0.0.0.0 --port $PORT
