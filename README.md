# Todays paper

## How to run docker image

```bash
docker run -p 8000:8000 -it --rm registry.gitlab.com/todays-paper/web:latest
```

## How to run development docker image

```bash
docker run -p 8000:8000 -it --rm -v"$(pwd):/app" registry.gitlab.com/todays-paper/web:latest
```

## How to run local development

```bash
virtualenv -p python3 venv
source venv/bin/activate
pip install -e .
uvicorn paper:app --reload --host 0.0.0.0
```

## Usage

Fetch eInk image by requesting the URL `/paper` or PNG image by including the optional querystring `format=png`.
